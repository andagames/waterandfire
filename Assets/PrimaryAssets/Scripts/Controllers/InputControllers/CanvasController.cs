﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasController : MonoBehaviour {
    public Camera cam;
    public GameObject startButton;
    public GameObject backButton;
    public GameObject optionsButton;
    // Use this for initialization
    void Start () {
        cam = GameObject.Find("CanvasCamera").GetComponent<Camera>();

    }

    // Update is called once per frame
    void Update () {
        RaycastHit2D hit = Physics2D.Raycast(cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (hit.collider.name.Equals("StartButton"))
                {
                    GameController.isGameStarted = true;
                    GameController.isGameOver = false;
                    startButton.SetActive(false);
                    backButton.SetActive(false);
                    optionsButton.SetActive(false);

                }
                else if(hit.collider.name.Equals("BackButton")){
                    SceneManager.LoadScene("MenuScene");

                }
                else if (hit.collider.name.Equals("OptionButton")){

                }
            }
        }



        if (GameController.isGameOver)
        {
            startButton.SetActive(true);
            backButton.SetActive(true);
            optionsButton.SetActive(true);

        }
    }
}
