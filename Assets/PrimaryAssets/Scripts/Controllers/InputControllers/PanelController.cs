﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour
{
    public static bool isLeftTouched = false;
    public static bool isLeftReleased = false;
    public static bool isLeft = false;
    public static bool isRightTouched = false;
    public static bool isRightReleased = false;
    public static bool isRight = false;
    public Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.Find("CanvasCamera").GetComponent<Camera>();

    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void onLeftPanelDown()
    {
        Debug.Log("onLeftPanelDown");
        HolderController.leftCubeResting = false;
        SideHolderController.leftCubeFringed = false;
        isLeftTouched = true;
        isLeftReleased = false;
        isLeft = true;
    }

    public void onLeftPanelUp()
    {
        Debug.Log("onLeftPanelUp");
        HolderController.leftCubeResting = false;
        SideHolderController.leftCubeFringed = false;
        isLeftTouched = false;
        isLeftReleased = true;
        isLeft = false;
    }

    public void onRightPanelDown()
    {
        Debug.Log("onRightPanelDown");
        HolderController.rightCubeResting = false;
        SideHolderController.rightCubeFringed = false;
        isRightTouched = true;
        isRightReleased = false;
        isRight = true;
    }
    public void onRightPanelUp()
    {
        HolderController.rightCubeResting = false;
        SideHolderController.rightCubeFringed = false;
        isRightTouched = false;
        isRightReleased = true;
        isRight = false;
    }
}
