﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGeneratorController : MonoBehaviour
{
    public GameObject obstaclePrefab;
    public GameObject obstacles;
    private bool generationLock = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.isGameStarted)
        {
            if (!generationLock)
            {
                StartRightObstacleGeneration();
                StartLeftObstacleGeneration();
                generationLock = true;
            }

        }else if (GameController.isGameOver)
        {
            StopAllCoroutines();
            generationLock = false;
        }
    }
    public void StartRightObstacleGeneration()
    {
        StartCoroutine(GenerateRightObstacles());
    }

    public IEnumerator GenerateRightObstacles()
    {
        while (true)
        {
            GameObject obstacleClone = Instantiate(obstaclePrefab) as GameObject;
            obstacleClone.transform.SetParent(this.transform);
            obstacleClone.transform.localPosition = getRandomRightPosition();
            obstacleClone.transform.localScale = Constants.obstacleScale;
            yield return new WaitForSeconds(Constants.secondsPerGeneration);
        }
    }

    public void StartLeftObstacleGeneration()
    {
        StartCoroutine(GenerateLeftObstacles());
    }

    public IEnumerator GenerateLeftObstacles()
    {
        while (true)
        {
            GameObject obstacleClone = Instantiate(obstaclePrefab) as GameObject;
            obstacleClone.transform.SetParent(this.transform);
            obstacleClone.transform.localPosition = getRandomLeftPosition();
            obstacleClone.transform.localScale = Constants.obstacleScale;
            yield return new WaitForSeconds(Constants.secondsPerGeneration);
        }
    }

    public Vector3 getRandomRightPosition()
    {
        Vector3 vectorRight = new Vector3(0f, 0f, 0f);
        float randomX = UnityEngine.Random.Range(0.57f, 2.99f);
        float randomZ = UnityEngine.Random.Range(20f, 22f);
        vectorRight = new Vector3(randomX, 0.8f, this.transform.position.z + randomZ);
        return vectorRight;
    }

    public Vector3 getRandomLeftPosition()
    {
        Vector3 vectorLeft = new Vector3(0f, 0f, 0f);
        float randomX = UnityEngine.Random.Range(-3.23f, -0.85f);
        float randomZ = UnityEngine.Random.Range(20f, 22f);
        vectorLeft = new Vector3(randomX, 0.8f, this.transform.position.z + randomZ);
        return vectorLeft;
    }
}