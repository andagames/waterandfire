﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour {

    
	// Use this for initialization
	void Start () {
        this.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

    }

    // Update is called once per frame
    void Update () {
    }



    void OnTriggerEnter(Collider collider)
    {
       // Debug.Log(collider.gameObject.name + "çarpışıyor");
        if (collider.gameObject.name == "RightCube"  || collider.gameObject.name == "LeftCube")
        {
            GameController.isGameStarted = false;
            GameController.isGameOver = true;
        }
    }
}
