﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideHolderController : MonoBehaviour {

    public static bool leftCubeFringed = false;
    public static bool rightCubeFringed = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
     
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "LeftCube")
        {
            leftCubeFringed = true;
            //Debug.Log("Sol küp açılmış");
        }
        if (collision.gameObject.name == "RightCube")
        {
            rightCubeFringed = true;
            //Debug.Log("sağ küp açılmış");

        }
    }


    void OnCollisionStayed()
    {
       
    }
}
