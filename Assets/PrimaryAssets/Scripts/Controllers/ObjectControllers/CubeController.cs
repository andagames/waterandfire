﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour {

    public GameObject leftCube;
    public GameObject rightCube;
    private bool rightVelocityLock = false;
    private bool leftVelocityLock = true;

    private bool rightCubeStill = true;
    private bool leftCubeStill = true;
    private bool initialSpeedLock = true;
  //  private Vector3 rightInitialPosition;
   // private Vector3 leftInitialPosition;

    // Use this for initialization
    void Start () {
       // rightInitialPosition = rightCube.transform.localPosition;
       // leftInitialPosition = leftCube.transform.localPosition;
       // this.gameObject.GetComponent<Rigidbody>().velocity = Constants.forwardVector;

    }
   

    // Update is called once per frame
    void Update()
    {
        if (GameController.isGameOver)
        {

            rightCube.GetComponent<Rigidbody>().velocity = Constants.zeroVector;
            leftCube.GetComponent<Rigidbody>().velocity = Constants.zeroVector;
            initialSpeedLock = true;

        } 
        else if (GameController.isGameStarted)
        {
            if (initialSpeedLock)
            {
                relocatePosition();
                resetCurrentVelocity();
                rightCube.gameObject.GetComponent<Rigidbody>().velocity = Constants.forwardVector;
                leftCube.gameObject.GetComponent<Rigidbody>().velocity = Constants.forwardVector;
                initialSpeedLock = false;
            }
            #region InputHandler
            this.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            //this.GetComponent<Rigidbody>().velocity = Constants.forwardVector;

            if (PanelController.isRightTouched)
            {
                if (!SideHolderController.rightCubeFringed)
                {
                    if (rightCubeStill)
                    {
                        rightCube.GetComponent<Rigidbody>().velocity = Constants.rightAdditionVector;
                      //  Debug.Log("sağ gidiyor");
                    }
                    else
                    {
                        rightCubeStill = true;
                        rightCube.GetComponent<Rigidbody>().velocity = Constants.forwardVector;
                       // Debug.Log("sağ stilled after moving while touching.");
                    }
                }
                else if (SideHolderController.rightCubeFringed)
                {
                    rightCube.transform.localPosition = Constants.rightFringedPosition;
                    rightCubeStill = false;
                    rightCube.GetComponent<Rigidbody>().velocity = Constants.forwardVector;
                   // Debug.Log("sağ fringing");
                }
            }
            else if (PanelController.isRightReleased)
            {
                if (!HolderController.rightCubeResting)
                {

                    rightCube.GetComponent<Rigidbody>().velocity = Constants.leftAdditionVector;

                    /*
                                    if (rightCubeStill)
                                    {
                                        rightCube.GetComponent<Rigidbody>().velocity = Constants.forwardVector;
                                        Debug.Log("sağ stilled after moving while not touching.");
                                    }
                                    else
                                    {
                                        rightCube.GetComponent<Rigidbody>().velocity = Constants.leftAdditionVector;

                                    }
                                    */
                }
                else if (HolderController.rightCubeResting)
                {
                    rightCube.transform.localPosition = Constants.rightInitialPosition;
                    rightCube.GetComponent<Rigidbody>().velocity = Constants.forwardVector;
                    //Debug.Log("sağ resting");
                }

            }
            if (PanelController.isLeftTouched)
            {
                if (!SideHolderController.leftCubeFringed)
                {
                    if (leftCubeStill)
                    {
                        leftCube.GetComponent<Rigidbody>().velocity = Constants.leftAdditionVector;
                        //Debug.Log("sol gidiyor");
                    }
                    else
                    {
                        leftCube.GetComponent<Rigidbody>().velocity = Constants.forwardVector;
                        //Debug.Log("sol stilled after moving while touching.");
                        leftCubeStill = true;

                    }
                }
                else if (SideHolderController.leftCubeFringed)
                {
                    leftCube.transform.localPosition = Constants.leftFringedPosition;
                    leftCubeStill = false;
                    leftCube.GetComponent<Rigidbody>().velocity = Constants.forwardVector;
                    //Debug.Log("sol fringing");
                }
            }
            else if (PanelController.isLeftReleased)
            {

                if (!HolderController.leftCubeResting)
                {
                    leftCube.GetComponent<Rigidbody>().velocity = Constants.rightAdditionVector;

                    /*
                    if (leftCubeStill)
                    {
                        leftCube.GetComponent<Rigidbody>().velocity = Constants.forwardVector;
                        Debug.Log("sol stilled after moving while not touching.");

                    }
                    else
                    {
                        leftCube.GetComponent<Rigidbody>().velocity = Constants.rightAdditionVector;
                        Debug.Log("sol dönüyor");
                    }

                    */

                }
                if (HolderController.leftCubeResting)
                {
                    leftCube.transform.localPosition = Constants.leftInitialPosition;
                    leftCube.GetComponent<Rigidbody>().velocity = Constants.forwardVector;
                    //Debug.Log("sol resting");

                }
            }
            else
            {
                leftCube.GetComponent<Rigidbody>().velocity = Constants.forwardVector;

            }
            #endregion
        }
    }

    private void relocatePosition()
    {
        rightCube.transform.localPosition = Constants.rightInitialPosition;
        leftCube.transform.localPosition = Constants.leftInitialPosition;

    }
    private void resetCurrentVelocity()
    {
        rightCube.GetComponent<Rigidbody>().velocity = Constants.zeroVector;
        leftCube.GetComponent<Rigidbody>().velocity = Constants.zeroVector;

    }
}

