﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathGeneratorController : MonoBehaviour {

    public GameObject pathPrefab;
    private Vector3 generatorPosition;
    private Vector3 forwardVector;
    private bool generationLock = false;
     
    // Use this for initialization
    void Start () {
        generationLock = true;
        forwardVector = new Vector3(0f, 0f, 37.595f);
       // generatorPosition = this.GetComponentInParent<Transform>().position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    void OnTriggerEnter(Collider collider)
    {
        //Debug.Log("triggering object:" +collider.gameObject.name);
        if (collider.gameObject.name.Equals("PathFollower")&& generationLock )
        {
            //Debug.Log("GENERATION TRIGGERD!!!");
            Instantiate(pathPrefab, forwardVector + GameController.previousPathPosition, Quaternion.identity);
            GameController.previousPathPosition = forwardVector + GameController.previousPathPosition;
            generationLock = false;
        }
    }
}
