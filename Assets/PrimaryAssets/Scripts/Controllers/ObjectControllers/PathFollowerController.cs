﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollowerController : MonoBehaviour {

    private bool isPositionInitial = false;
	// Use this for initialization
	void Start () {
        // this.GetComponent<Rigidbody>().velocity = Constants.forwardVector;

    }

    // Update is called once per frame
    void Update () {
        if (GameController.isGameOver)
        {
            this.GetComponent<Rigidbody>().velocity = Constants.zeroVector;
            isPositionInitial = false;

        }
        else if (GameController.isGameStarted)
        {
            if(!isPositionInitial)
            {
                this.transform.position = Constants.pathFollowerInitialPositon;
                isPositionInitial = true;
                this.GetComponent<Rigidbody>().velocity = Constants.forwardVector;

            }

        }
    }

    
}
