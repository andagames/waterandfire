﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public GameObject pathPrefab;
    public GameObject rightPanel;
    public GameObject leftPanel;
    public GameObject leftCube;
    public GameObject rightCube;
    public GameObject timerText;
    public static bool isGameStarted = false;
    public static bool isGameOver = false;
    public static Vector3 previousPathPosition;
    public int playerPoint = 0;
    public int seconds = 0;
    public int minutes = 0;
    public int hours = 0;
    public GameObject[] lefOverObstacles;
    public GameObject[] lefOverPaths;
    public GameObject[] lefOverFences;

    private bool isInitializationComplete = false;
    // Use this for initialization
    void Start()
    {
        previousPathPosition = new Vector3(0f, 0f, 0f);
        //Instantiate(pathPrefab, GameController.previousPathPosition, Quaternion.identity);
        leftPanel.SetActive(false);
        rightPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        if (isGameStarted)
        {
            if (!isInitializationComplete)
            {
                DestroyLeftOvers();
               // Debug.Log("GameStarted");
                previousPathPosition = new Vector3(0f, 0f, 0f);
                Instantiate(pathPrefab, GameController.previousPathPosition, Quaternion.identity);
                leftPanel.SetActive(true);
                rightPanel.SetActive(true);
                StartTimer();
                isInitializationComplete = true;
            }

        }
        else if (isGameOver)
        {
           // Debug.Log("GameOver");
            isInitializationComplete = false;
            isGameStarted = false;
            leftPanel.SetActive(false);
            rightPanel.SetActive(false);
            StopAllCoroutines();

        }
    }

    private void DestroyLeftOvers()
    {
        ClearObstacles();
        ClearPaths();
        ClearFences();
    }
    private void ClearFences()
    {
        lefOverFences = GameObject.FindGameObjectsWithTag("Fence");
        foreach (GameObject obstacle in lefOverObstacles)
        {
            Destroy(obstacle);
        }
    }
    private void ClearPaths()
    {
        lefOverPaths = GameObject.FindGameObjectsWithTag("Path");
        foreach (GameObject obstacle in lefOverObstacles)
        {
            Destroy(obstacle);
        }
    }

    public void ClearObstacles()
    {
        lefOverObstacles = GameObject.FindGameObjectsWithTag("Obstacle");
        foreach (GameObject obstacle in lefOverObstacles)
        {
            Destroy(obstacle);
        }

    }

    private void StartTimer()
    {
        //Debug.Log("StartTimer");
        seconds = 0;
        minutes = 0;
        hours = 0;
        timerText.gameObject.GetComponent<Text>().text = "0" + "0" + " : 0" + "0";
        StartCoroutine(Timer());
    }
    private IEnumerator Timer()
    {

        while (true)
        {
            yield return new WaitForSeconds(1);
            seconds++;
            if (seconds == 60)
            {
                minutes++;
                seconds = 0;
            }
            if (seconds < 10 && minutes < 10)
            {
                timerText.gameObject.GetComponent<Text>().text = "0" + minutes.ToString() + " : 0" + seconds.ToString();
            }
            else if (seconds < 10 && minutes > 9)
            {
                timerText.gameObject.GetComponent<Text>().text = minutes.ToString() + " : 0" + seconds.ToString();
            }
            else if (minutes < 10 && seconds > 9)
            {
                timerText.gameObject.GetComponent<Text>().text = "0" + minutes.ToString() + " : " + seconds.ToString();
            }
            else
            {
                timerText.gameObject.GetComponent<Text>().text = minutes.ToString() + " : " + seconds.ToString();
            }
        }
    }
}
