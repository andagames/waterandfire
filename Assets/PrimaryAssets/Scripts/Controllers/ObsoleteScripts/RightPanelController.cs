﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightPanelController : MonoBehaviour {

    public static bool isRightTouched = false;
    public static bool isRightReleased = false;
    public static bool isRight = false;
    public Camera cam;

    // Use this for initialization
    void Start () {
        cam = GameObject.Find("CanvasRightCamera").GetComponent<Camera>();

    }



    // Update is called once per frame
    void Update () {
        #region Old Input
        
        RaycastHit2D hit = Physics2D.Raycast(cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (hit.collider.name.Equals("RightPanel"))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    // SideHolderController.rightCubeFringed = false;
                    //Debug.Log("current hit: " + hit.collider.name);
                 //   if (hit.collider.name.Equals("RightPanel"))
                 //   {
                        HolderController.rightCubeResting = false;
                        SideHolderController.rightCubeFringed = false;

                        isRightTouched = true;
                        isRightReleased = false;
                        isRight = true;
                    if (LeftPanelController.isleft)
                    {
                        HolderController.leftCubeResting = false;
                        SideHolderController.leftCubeFringed = false;

                        LeftPanelController.isLeftTouched = true;
                        LeftPanelController.isLeftReleased = false;
                        LeftPanelController.isleft = true;
                    }
                    // Debug.Log("right touched");

                    /*
                    if (Input.GetMouseButton(0))
                    {
                        if (hit.collider.name.Equals("LeftPanel"))
                        {
                           

                                // Debug.Log("current hit: " + hit.collider.name);
                                // if (hit.collider.name.Equals("LeftPanel"))
                                // {
                                HolderController.leftCubeResting = false;
                                SideHolderController.leftCubeFringed = false;

                                LeftPanelController.isLeftTouched = true;
                            LeftPanelController.isLeftReleased = false;
                            LeftPanelController.isleft = true;
                                //   Debug.Log("left touched");
                                //}
                            
                        

                        }
                    }
                    */
                    //   }
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    if (isRight )
                    {
                        if (hit.collider.name.Equals("RightPanel") || hit.collider.name.Equals("LeftPanel"))
                        {
                            HolderController.rightCubeResting = false;
                            SideHolderController.rightCubeFringed = false;
                            isRightTouched = false;
                            isRightReleased = true;
                            isRight = false;
                            Debug.Log("right released");
                        }
                   }
                    if (LeftPanelController.isleft)
                    {
                        HolderController.leftCubeResting = false;
                        SideHolderController.leftCubeFringed = false;
                        LeftPanelController.isLeftTouched = false;
                        LeftPanelController.isLeftReleased = true;
                        LeftPanelController.isleft = false;
                        Debug.Log("left released");
                    }

                }
            }





            


            








            /*
            else if (Input.touchCount < 2)
            {
                /*
             if (hit.collider.name.Equals("RightPanel"))
                {
                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;

                    isRightTouched = true;
                    isRightReleased = false;
                    isRight = true;
                    // Debug.Log("right touched");
                }
                
                if (Input.GetMouseButtonDown(0))
                {
                    // SideHolderController.rightCubeFringed = false;
                    //Debug.Log("current hit: " + hit.collider.name);
                    if (hit.collider.name.Equals("RightPanel"))
                    {
                        HolderController.rightCubeResting = false;
                        SideHolderController.rightCubeFringed = false;

                        isRightTouched = true;
                        isRightReleased = false;
                        isRight = true;
                        // Debug.Log("right touched");
                    }
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    if (isRight)
                    {
                        HolderController.rightCubeResting = false;
                        SideHolderController.rightCubeFringed = false;
                        isRightTouched = false;
                        isRightReleased = true;
                        isRight = false;
                        Debug.Log("right released");

                    }

                }
            }
            else if (Input.touchCount > 0)
            {
                /*
             if (hit.collider.name.Equals("RightPanel"))
                {
                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;

                    isRightTouched = true;
                    isRightReleased = false;
                    isRight = true;
                    // Debug.Log("right touched");
                }
                
                if (Input.GetMouseButtonDown(0))
                {
                    // SideHolderController.rightCubeFringed = false;
                    //Debug.Log("current hit: " + hit.collider.name);
                    if (hit.collider.name.Equals("RightPanel"))
                    {
                        HolderController.rightCubeResting = false;
                        SideHolderController.rightCubeFringed = false;

                        isRightTouched = true;
                        isRightReleased = false;
                        isRight = true;
                        // Debug.Log("right touched");
                    }
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    if (isRight)
                    {
                        HolderController.rightCubeResting = false;
                        SideHolderController.rightCubeFringed = false;
                        isRightTouched = false;
                        isRightReleased = true;
                        isRight = false;
                        Debug.Log("right released");

                    }

                }
            }
        }
    */

            #endregion


        }

}

/*
    public void OnMouseDown()
    {
        HolderController.rightCubeResting = false;
        SideHolderController.rightCubeFringed = false;

        isRightTouched = true;
        isRightReleased = false;
        isRight = true;
        // Debug.Log("right touched");
    }

    public void OnMouseUp()
    {
        if (isRight)
        {
            HolderController.rightCubeResting = false;
            SideHolderController.rightCubeFringed = false;
            isRightTouched = false;
            isRightReleased = true;
            isRight = false;
            Debug.Log("right released");

        }
    }
    */
}
