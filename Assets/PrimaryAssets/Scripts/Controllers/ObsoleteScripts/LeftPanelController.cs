﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftPanelController : MonoBehaviour {

    public static bool isLeftTouched = false;
    public static bool isLeftReleased = false;
    public static bool isleft = false;
    public Camera cam;
    private static bool rightLock = false;
    // Use this for initialization
    void Start () {
        cam = GameObject.Find("CanvasLeftCamera").GetComponent<Camera>();   

    }


    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (hit.collider.name.Equals("RightPanel"))
            {
                rightLock = true;
              
            }

            if (Input.GetMouseButtonDown(0))
            {

                if (hit.collider.name.Equals("LeftPanel"))
                {
                    // Debug.Log("current hit: " + hit.collider.name);
                    // if (hit.collider.name.Equals("LeftPanel"))
                    // {
                    HolderController.leftCubeResting = false;
                    SideHolderController.leftCubeFringed = false;

                    isLeftTouched = true;
                    isLeftReleased = false;
                    isleft = true;
                    //   Debug.Log("left touched");
                    //}
                    if (rightLock)
                    {
                        HolderController.rightCubeResting = false;
                        SideHolderController.rightCubeFringed = false;

                        RightPanelController.isRightTouched = true;
                        RightPanelController.isRightReleased = false;
                        RightPanelController.isRight = true;
                    }
                }
            }
            else if (Input.GetMouseButton(0))
            {
                if (hit.collider.name.Equals("RightPanel"))
                {
                    rightLock = true;

                }
                if (isleft&&rightLock)
                {
                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;

                    RightPanelController.isRightTouched = true;
                    RightPanelController.isRightReleased = false;
                    RightPanelController.isRight = true;
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {

                if (isleft)
                {
                    if (hit.collider.name.Equals("RightPanel") || hit.collider.name.Equals("LeftPanel"))
                    {
                        HolderController.leftCubeResting = false;
                        SideHolderController.leftCubeFringed = false;
                        isLeftTouched = false;
                        isLeftReleased = true;
                        isleft = false;
                        rightLock = false;
                        Debug.Log("left released");
                    }
                }
                if (RightPanelController.isRight)
                {
                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;
                    RightPanelController.isRightTouched = false;
                    RightPanelController.isRightReleased = true;
                    RightPanelController.isRight = false;
                    rightLock = false;
                    Debug.Log("right released");
                }


            

                
            }


        }





            #region Old Input
            /*
                    RaycastHit2D hit = Physics2D.Raycast(cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                    if (hit.collider != null)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {

                            // Debug.Log("current hit: " + hit.collider.name);
                            if (hit.collider.name.Equals("LeftPanel"))
                            {
                                HolderController.leftCubeResting = false;
                                SideHolderController.leftCubeFringed = false;

                                isLeftTouched = true;
                                isLeftReleased = false;
                                isleft = true;
                                //   Debug.Log("left touched");
                            }
                        }
                        else if (Input.GetMouseButtonUp(0))
                        {

                            if (isleft)
                            {
                                HolderController.leftCubeResting = false;
                                SideHolderController.leftCubeFringed = false;
                                isLeftTouched = false;
                                isLeftReleased = true;
                                isleft = false;
                                Debug.Log("left released");

                            }


                        }

                        if(Input.touchCount >0)
                        {
                            if(Input.GetTouch().fingerId)
                            if (hit.collider.name.Equals("LeftPanel"))
                            {
                                HolderController.leftCubeResting = false;
                                SideHolderController.leftCubeFringed = false;

                                isLeftTouched = true;
                                isLeftReleased = false;
                                isleft = true;
                                //   Debug.Log("left touched");
                            }

                            if (Input.GetMouseButtonDown(0))
                            {

                                // Debug.Log("current hit: " + hit.collider.name);
                                if (hit.collider.name.Equals("LeftPanel"))
                                {
                                    HolderController.leftCubeResting = false;
                                    SideHolderController.leftCubeFringed = false;

                                    isLeftTouched = true;
                                    isLeftReleased = false;
                                    isleft = true;
                                    //   Debug.Log("left touched");
                                }
                            }
                            else if (Input.GetMouseButtonUp(0))
                            {

                                if (isleft)
                                {
                                    HolderController.leftCubeResting = false;
                                    SideHolderController.leftCubeFringed = false;
                                    isLeftTouched = false;
                                    isLeftReleased = true;
                                    isleft = false;
                                    Debug.Log("left released");

                                }


                            }


                        }
                        else if (Input.touchCount > 1 )
                        {
                            if (Input.GetMouseButtonDown(0))
                            {

                                // Debug.Log("current hit: " + hit.collider.name);
                                if (hit.collider.name.Equals("LeftPanel"))
                                {
                                    HolderController.leftCubeResting = false;
                                    SideHolderController.leftCubeFringed = false;

                                    isLeftTouched = true;
                                    isLeftReleased = false;
                                    isleft = true;
                                    //   Debug.Log("left touched");
                                }
                            }
                            else if (Input.GetMouseButtonUp(0))
                            {

                                if (isleft)
                                {
                                    HolderController.leftCubeResting = false;
                                    SideHolderController.leftCubeFringed = false;
                                    isLeftTouched = false;
                                    isLeftReleased = true;
                                    isleft = false;
                                    Debug.Log("left released");

                                }


                            }


                        }
                    }


            */
            #endregion
        }
    

    }
    /*
    public void OnMouseDown()
    {   
        HolderController.leftCubeResting = false;
        SideHolderController.leftCubeFringed = false;

        isLeftTouched = true;
        isLeftReleased = false;
        isleft = true;
        
        if (RightPanelController.isRight)
        {
            HolderController.rightCubeResting = false;
            SideHolderController.rightCubeFringed = false;

            RightPanelController.isRightTouched = true;
            RightPanelController.isRightReleased = false;
            RightPanelController.isRight = true;
            Debug.Log("right released from left");

        }
        
    }

    public void OnMouseUp()
    {
        if (isleft)
        {
            HolderController.leftCubeResting = false;
            SideHolderController.leftCubeFringed = false;
            isLeftTouched = false;
            isLeftReleased = true;
            isleft = false;
            Debug.Log("left released");
        

        }

    }
    */
