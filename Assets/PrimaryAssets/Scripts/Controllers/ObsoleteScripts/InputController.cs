﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    public static bool isLeftTouched = false;
    public static bool isLeftReleased = false;
    public static bool isLeft = false;
    public static bool isRightTouched = false;
    public static bool isRightReleased = false;
    public static bool isRight = false;
    public Camera rightcam;
    public Camera leftcam;


    // Use this for initialization
    void Start()
    {
        rightcam = GameObject.Find("CanvasRightCamera").GetComponent<Camera>();
        leftcam = GameObject.Find("CanvasLeftCamera").GetComponent<Camera>();

    }


    // Update is called once per frame
    void Update()
    {
        RaycastHit2D rightHit = Physics2D.Raycast(rightcam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        RaycastHit2D leftHit = Physics2D.Raycast(leftcam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        if (rightHit.collider != null)
        {
            if (rightHit.collider.name.Equals("RightPanel"))
            {
                isRight = true;
                Debug.Log("isRight = true");

            }

            if (Input.GetMouseButtonDown(0))
            {

                if (rightHit.collider.name.Equals("RightPanel"))
                {
                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;
                    isRightTouched = true;
                    isRightReleased = false;
                    isRight = true;
                     Debug.Log("rightHit RightPanel GetMouseButtonDown");

                }
                if (leftHit.collider.name.Equals("LeftPanel"))
                {
                    HolderController.leftCubeResting = false;
                    SideHolderController.leftCubeFringed = false;
                    isLeftTouched = true;
                    isLeftReleased = false;
                    isLeft = true;
                    Debug.Log("rightHit LeftPanel GetMouseButtonDown");

                }
            }
            else if (Input.GetMouseButton(0))
            {
                 if (isRight)
                {
                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;
                    isRightTouched = true;
                    isRightReleased = false;
                    isRight = true;
                    Debug.Log("rightHit isRight GetMouseButton");

                }
                else if (isLeft && isRight)
                {
                    HolderController.leftCubeResting = false;
                    SideHolderController.leftCubeFringed = false;
                    isLeftTouched = true;
                    isLeftReleased = false;
                    isLeft = true;

                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;
                    isRightTouched = true;
                    isRightReleased = false;
                    isRight = true;
                    Debug.Log("rightHit isRight isLeft GetMouseButton");

                }

            }
            else if (Input.GetMouseButtonUp(0))
            {

                if (isLeft)
                {
                    HolderController.leftCubeResting = false;
                    SideHolderController.leftCubeFringed = false;
                    isLeftTouched = false;
                    isLeftReleased = true;
                    isLeft = false;
                    Debug.Log("rightHit isLeft GetMouseButtonUp");

                }
                if (isRight)
                {
                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;
                    isRightTouched = false;
                    isRightReleased = true;
                    isRight = false;
                    Debug.Log("rightHit isRight GetMouseButtonUp");

                }

            }

        }


        if (leftHit.collider != null)
        {
            if (leftHit.collider.name.Equals("LeftPanel"))
            {
                isLeft = true;
                Debug.Log("isLeft = true");

            }

            if (Input.GetMouseButtonDown(0))
            {

                if (leftHit.collider.name.Equals("LeftPanel"))
                {
                    HolderController.leftCubeResting = false;
                    SideHolderController.leftCubeFringed = false;
                    isLeftTouched = true;
                    isLeftReleased = false;
                    isLeft = true;
                    Debug.Log("leftHit isLeft GetMouseButtonUp");

                }
                if (rightHit.collider.name.Equals("RightPanel"))
                {
                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;
                    isRightTouched = true;
                    isRightReleased = false;
                    isRight = true;
                    Debug.Log("leftHit isRight GetMouseButtonUp");

                }
            }
            else if (Input.GetMouseButton(0))
            {        
                if (isLeft)
                {
                    HolderController.leftCubeResting = false;
                    SideHolderController.leftCubeFringed = false;
                    isLeftTouched = true;
                    isLeftReleased = false;
                    isLeft = true;
                    Debug.Log("leftHit isLeft GetMouseButton");

                }
                else if (isLeft && isRight)
                {
                    HolderController.leftCubeResting = false;
                    SideHolderController.leftCubeFringed = false;
                    isLeftTouched = true;
                    isLeftReleased = false;
                    isLeft = true;

                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;
                    isRightTouched = true;
                    isRightReleased = false;
                    isRight = true;
                    Debug.Log("leftHit isRight isLeft GetMouseButton");

                }

            }
            else if (Input.GetMouseButtonUp(0))
            {

                if (isLeft)
                {
                    HolderController.leftCubeResting = false;
                    SideHolderController.leftCubeFringed = false;
                    isLeftTouched = false;
                    isLeftReleased = true;
                    isLeft = false;
                    Debug.Log("leftHit isLeft GetMouseButtonUp");

                }
                else if (isRight)
                {
                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;
                    isRightTouched = false;
                    isRightReleased = true;
                    isRight = false;
                    Debug.Log("leftHit isRight GetMouseButtonUp");

                }
                else if (isLeft && isRight)
                {
                    HolderController.leftCubeResting = false;
                    SideHolderController.leftCubeFringed = false;
                    isLeftTouched = true;
                    isLeftReleased = false;
                    isLeft = true;

                    HolderController.rightCubeResting = false;
                    SideHolderController.rightCubeFringed = false;
                    isRightTouched = true;
                    isRightReleased = false;
                    isRight = true;
                    Debug.Log("leftHit isRight isLeft GetMouseButtonUp");

                }

            }

        }

    }

}







