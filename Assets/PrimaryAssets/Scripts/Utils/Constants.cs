﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour {

    public static float midPointValue = 0.0f;
    public static float secondsPerGeneration = 1;
    public static float speed = 6f;
    public static Vector3 rightAdditionVector = new Vector3(10f, 0f, speed);
    public static Vector3 leftAdditionVector = new Vector3(-10f, 0f, speed);
    public static Vector3 zeroVector = new Vector3(0f, 0f, 0f);
    public static Vector3 forwardVector = new Vector3(0f, 0f, speed);
    public static Quaternion lockRotQuaternion = new Quaternion(0f, 0f, 0f,0f);
    public static Vector3 rightInitialPosition = new Vector3(0.44f, 0.7f, -2.44f);
    public static Vector3 leftInitialPosition = new Vector3(-0.7f, 0.7f, -2.44f);
    public static Vector3 rightFringedPosition = new Vector3(3.18f,0.7f, -2.44f);
    public static Vector3 leftFringedPosition = new Vector3(-3.448f, 0.7f, -2.44f);
    public static Vector3 pathFollowerInitialPositon = new Vector3(0.1434717f, -0.4470937f, 3.248237f);

    public static Vector3 obstacleRRPosition = new Vector3(2.99f, 0.8f, 0);
    public static Vector3 obstacleRLPosition = new Vector3(0.57f, 0.8f, 0);
    public static Vector3 obstacleLLPosition = new Vector3(-3.23f, 0.8f, 0);
    public static Vector3 obstacleLRPosition = new Vector3(-0.85f, 0.8f, 0);
    public static Vector3 obstacleScale = new Vector3(0.9549497f, 0.6732123f, 0.6732123f);
}
